var LHWLWidget = (function(){

    const URL = "http://localhost:8092/build/frame.js";

    var iframe;
    var isShowFrame;

    window.LeadHitWidgetOnLoadFrame = function(frameString){
        div = document.createElement("div");
        div.innerHTML = frameString;
        iframe = div.getElementsByTagName("iframe")[0];
        if(isShowFrame) iframe.style.display = "block";
        document.body.appendChild(iframe);
        function messageHandler(event){
            if(event.source === iframe.contentWindow && event.data === "close"){
                var parent = iframe.parentNode;
                parent.removeChild(iframe);
                window.removeEventListener("message", messageHandler);
                window.document.body.removeChild(frameScript);
            }
        }
        window.addEventListener("message", messageHandler)
    };

    var frameScript;
    function GET_SCRIPT(url){
        frameScript = document.createElement("script");
        frameScript.src = url;
        document.body.appendChild(frameScript);
    }

    var isOnLoadExecuted = false;
    function onLoadHandler(event){
        if(isOnLoadExecuted) return;
        GET_SCRIPT(URL);
        isOnLoadExecuted = true;
    }

    document.addEventListener("DOMContentLoaded", onLoadHandler, false);
    document.addEventListener("load", onLoadHandler, false);


    function LHWLWidget(){

    }
    LHWLWidget.show = function(){
        isShowFrame = true;
        if(iframe) iframe.style.display = "block"
    };
    return LHWLWidget;
})();


LHWLWidget.show();