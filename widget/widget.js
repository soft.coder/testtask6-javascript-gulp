(function(){
    var isLoaded;
    function onLoadFrameHandler(){
        if(isLoaded) return;

        var btnSend = document.leadHitForm.btnSend;
        var txtEmail = document.leadHitForm.email;
        var msgError = document.getElementById("errorMsg");
        var btnClose = document.getElementById("btnClose");

        var emailRegExp = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

        btnSend.addEventListener("click", function(event){
            if(window.lh_sf && typeof window.lh_sf === "function") {
                window.lh_sf(document.leadHitForm);
            }
        });
        txtEmail.addEventListener("keyup", function(event){
            if(!emailRegExp.test(event.target.value)){
                btnSend.setAttribute("disabled", "disabled");
                msgError.style.visibility = "visible";
            }
            else{
                btnSend.removeAttribute("disabled", "disabled");
                msgError.style.visibility = "hidden";

            }
        });

        btnClose.addEventListener("click", function(event){
           window.parent.postMessage("close", "*");
        });

        isLoaded = true;
    }
    document.addEventListener("DOMContentLoaded", onLoadFrameHandler, false);
    document.addEventListener("load", onLoadFrameHandler, false);
})();

window.lh_sf = function(form){
    console.log(form);
};