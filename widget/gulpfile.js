var fs = require('fs');
var gulp = require('gulp');
var gulpUtil = require('gulp-util');
var mustache = require('mustache');

var paths = {
    mustache: {
        html: ['widget.@(css|html|js)', 'widget-bundle.html', 'widget-frame.html'],
        json: 'widget.json'
    }
};

var mustacheTags = {
    frame: {},
    html: {},
    css: {},
    js: {}
};
const onLoadHandlerName = "LeadHitWidgetOnLoadFrame";

gulp.task('mustache-html', function(){
    function mustacheRender(filePath, tags){
        return new Promise( function(resolve, reject) {
            fs.readFile(filePath, 'utf8', function (err, data) {
                if (err){
                    reject(err);
                    throw err;
                }
                resolve(mustache.render(data, tags));
            })
        });

    }

    var frame = mustacheRender('./widget-frame.html', mustacheTags.frame);
    frame.then(function(result){
        result = result.replace(/"/g, '\\"');
        result = result.replace(/\r/g, '');
        result = result.replace(/\n/g, '');
        result = onLoadHandlerName + '("' + result + '")';
        fs.writeFile('./build/frame.js', result);
    });

    var tags =  [
        mustacheRender('./widget.css', mustacheTags.css),
        mustacheRender('./widget.js', mustacheTags.js),
        mustacheRender('./widget.html', mustacheTags.html)
    ];

    Promise.all(tags).then(function(results){
        var bundle = mustacheRender('./widget-bundle.html', {
            css: results[0],
            js: results[1],
            html: results[2]
        });
        bundle.then(function(bundle){
            fs.writeFile('./build/widget.html', bundle, function(err){
                if(err) throw err;
                //gulpUtil.log(bundle);
            });
        });

    });
});

gulp.task('mustache-json', function(){
    function parseTags(tagGroupSource, tagGroupDest){
        if(json[tagGroupSource] && json[tagGroupSource] instanceof Array && json[tagGroupSource].length > 0){
            json[tagGroupSource].forEach(function(tag){
                mustacheTags[tagGroupDest][tag.name] = tag.value;
            });
        }
    }
    var json = JSON.parse(fs.readFileSync('widget.json', 'utf8'));
    parseTags('frame_vars', 'frame');
    parseTags('html_vars', 'html');
    parseTags('css_vars', 'css');
    parseTags('js_vars', 'js');
});

gulp.task('default', ['mustache-json', 'mustache-html'], function() {
    gulp.watch(paths.mustache.html, ['mustache-html']);
    gulp.watch(paths.mustache.json, ['mustache-json', 'mustache-html']);
});

gulp.task('compile', ['mustache-json', 'mustache-html']);